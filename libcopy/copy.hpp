#ifndef __COPY_H_INCLUDED__
#define __COPY_H_INCLUDED__

#include <filesystem>
#include <spdlog/spdlog.h>

namespace libcopy
{
  void rename_or_copy(std::filesystem::path old_path, std::filesystem::path new_path, std::shared_ptr<spdlog::logger> logger, std::error_code &err);
}

#endif
