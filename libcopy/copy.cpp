#include "copy.hpp"
#include <fstream>
#include <spdlog/spdlog.h>

void libcopy::rename_or_copy(std::filesystem::path old_path, std::filesystem::path new_path, std::shared_ptr<spdlog::logger> logger, std::error_code &err)
{
  std::filesystem::create_directories(new_path.parent_path(), err);
  if (err)
  {
    logger->error("Creating parent directories: {}", err.message());
    return;
  }
  std::filesystem::rename(old_path, new_path, err);
  if (!err)
  {
    logger->info("Renamed file '{}' to '{}'", old_path.string(), new_path.string());
    return;
  }
  std::ifstream src(old_path, std::ios::binary);
  if (!src)
  {
    logger->error("Opening source file '{}'", old_path.string());
    return;
  }
  std::ofstream dst(new_path, std::ios::binary);
  if (!dst)
  {
    logger->error("Opening destination file '{}'", new_path.string());
    return;
  }
  if (!(dst << src.rdbuf()))
  {
    logger->error("Copying file '{}' to '{}'", old_path.string(), "?");
    return;
  };
  std::filesystem::remove(old_path, err);
  if (err)
  {
    logger->error("Removing file '{}': {}", old_path.string(), err.message());
    return;
  }
  logger->info("Copied file '{}' to '{}'", old_path.string(), new_path.string());
  return;
};
