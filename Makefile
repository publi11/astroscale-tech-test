build: deps cmake
	cd build && make

deps: dirs
	cd build && conan install ..

cmake: dirs
	cmake -B build

dirs:
	mkdir -p build

run:
	./build/SatelliteImageClassifier

time:
	time make run

clean: clean_build clean_files clean_log

clean_build:
	rm -Rf build

clean_files:
	rm -Rf data/*
	rm -Rf archive/*

clean_log:
	rm -f satelliteimageclassifier.log

DEFAULT_FILE_SIZE := 5000000 # 50MB
N_FILES := 10

seed_files:
	mkdir -p data
	tools/seed_files.sh ${DEFAULT_FILE_SIZE} ${N_FILES}

quick: clean build seed_files run
