#include <chrono>
#include <regex>
#include <libconfig/config.hpp>
#include <libcopy/copy.hpp>
#include <liblogger/logger.hpp>
#include <libwatcher/watcher.hpp>

int main()
{
  // Load configuration
  auto config = libconfig::Config();
  config.load_from_file("config.json");
  const auto source_dir = config.get_source_dir();
  const auto archive_dir = config.get_archive_dir();

  // Prepare logger
  liblogger::prepare_logger(config.get_log_file());
  auto logger = liblogger::get_logger();
  // Initiailse watcher and start
  auto watcher = libwatcher::Watcher(config.get_source_dir(), std::chrono::milliseconds(500));
  logger->info("Watching for files in '{}' and moving them to '{}'", config.get_source_dir().c_str(), config.get_archive_dir().c_str());
  watcher.start([logger, archive_dir](std::filesystem::path watched_file, libwatcher::FileStatus status, std::error_code &err) -> void {
    if (status == libwatcher::FileStatus::erased)
    {
      return;
    }
    // Filter images that match the spec filename
    auto old_filename = watched_file.filename().string();
    std::regex rgx("SAT_IMG_([0-9]{4})([0-9]{2})([0-9]{2})[0-9]{9}.raw");
    std::smatch matches;
    if (std::regex_search(old_filename, matches, rgx))
    {
      // Extract year, month and day from timtestamp
      std::string year = matches[1].str();
      std::string month = matches[2].str();
      std::string day = matches[3].str();
      // Construct new path
      std::filesystem::path new_path(archive_dir);
      new_path.append(year).append(month).append(day).append(old_filename);
      // Rename or copy the file
      libcopy::rename_or_copy(watched_file, new_path, logger, err);
      if (err)
      {
        logger->error("Renaming or copying: {}", err.message());
        logger->flush();
      }
    }
    else
    {
      logger->info("Skipping file '{}'", watched_file.string());
    }
    return;
  });
}
