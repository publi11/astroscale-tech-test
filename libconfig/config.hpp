#ifndef __CONFIG_H_INCLUDED__
#define __CONFIG_H_INCLUDED__

#include <filesystem>

namespace libconfig
{
  class Config
  {
  private:
    std::filesystem::path _source_dir;
    std::filesystem::path _archive_dir;
    std::filesystem::path _log_file;

  public:
    void load_from_file(std::filesystem::path config_file);
    std::filesystem::path get_source_dir();
    std::filesystem::path get_archive_dir();
    std::filesystem::path get_log_file();
  };
}

#endif
