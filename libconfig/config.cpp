#include <filesystem>
#include <fstream>
#include <nlohmann/json.hpp>
#include <libconfig/config.hpp>

using json = nlohmann::json;

void libconfig::Config::load_from_file(std::filesystem::path config_file)
{
  std::ifstream ifs(config_file);
  json config_json = json::parse(ifs);
  config_json.at("sourceDirectory").get_to(_source_dir);
  config_json.at("archiveDirectory").get_to(_archive_dir);
  if (config_json.contains("logFile"))
  {
    config_json.at("logFile").get_to(_log_file);
  }
  else
  {
    _log_file = "./default.log";
  }
};

std::filesystem::path libconfig::Config::get_source_dir()
{
  return _source_dir;
};

std::filesystem::path libconfig::Config::get_archive_dir()
{
  return _archive_dir;
};

std::filesystem::path libconfig::Config::get_log_file()
{
  return _log_file;
};
