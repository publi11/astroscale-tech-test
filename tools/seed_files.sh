#!/bin/bash

DEFAULT_FILE_SIZE=$1
N_FILES=$2

for i in $(seq 1 $N_FILES)
do
  YEAR=$[ ( $RANDOM % 10 )  + 2013 ]
  MONTH=$[ ( $RANDOM % 12 )  + 1 ]
  DAY=$[ ( $RANDOM % 28 )  + 1 ]
  HOUR=$[ ( $RANDOM % 24 ) ]
  MIN=$[ ( $RANDOM % 60 ) ]
  SEC=$[ ( $RANDOM % 60 ) ]
  MILLIS=$[ ( $RANDOM % 1000 ) ]
  YEAR=`printf "%04d" ${YEAR}`
  MONTH=`printf "%02d" ${MONTH}`
  DAY=`printf "%02d" ${DAY}`
  HOUR=`printf "%02d" ${HOUR}`
  MIN=`printf "%02d" ${MIN}`
  SEC=`printf "%02d" ${SEC}`
  MILLIS=`printf "%03d" ${MILLIS}`
  head -c ${DEFAULT_FILE_SIZE} /dev/random > data/SAT_IMG_${YEAR}${MONTH}${DAY}${HOUR}${MIN}${SEC}${MILLIS}.raw
done
