# Astroscale tech test

**Author:** Borja Lazaro Toralles

## Quick start

The project can be quickly set up using the provided VS Code container by:

1. Opening [VS Code](https://code.visualstudio.com/download).
2. Using the action `Remote-Containers: Clone Repository in Container Volume` and pasting this repo's URL.
3. Running `make quick`, which will clean any generated files, compile and link the executable, seed some initial files and run the program.

Alternatively if VS Code is not available, please install the dependencies defined in `.devcontainer/Dockerfile` and you should be able to build and run.

> NB: VS Code allows opening an existing directory on disk within the remote container by mounting it as a Docker volume. This option is NOT recommended since it slows down `inotify` and will affect the program performance in detecting large numbers of file changes.

## Approach

Given the performance constraint of having to move large files on disk, `libcopy::rename_or_copy` first attempts to just "rename" the file, which only affects the file descriptors and doesn't require moving the file's bytes around the disk. This approach however is limited to the source and destination paths being in the same file system. When this is not the case, it falls back to an in/out file stream copying, which is naturally slower. Since the task sheet only specifies that the source/archive folders are "on disk" but doesn't guarantee that they share a file system, both options have been implemented.

The latter behaviour can be tested by changing the provided `config.json` to use different file systems for `sourceDirectory` and `archiveDirectory` (I tested this with a mapped network drive on a local RPi4 using `samba`, but could be done as easily on a USB or second local disk or partition/volume).

Multi-threading was not implemeted after considering that writing to a single disk is intrinsically a serialized operation.

The `Makefile` contains a convenience random file generator that will emulate the satellite image streaming (default 50MB x 10 files) and can be configured with the following flags:

```
make seed_files DEFAULT_FILE_SIZE=5000000 N_FILES=10
```

The main application will fail if the configuration JSON is invalid. When running it will attempt to move any initial, created or modified files, logging any errors that may occur.
