#ifndef __WATCHER_H_INCLUDED__
#define __WATCHER_H_INCLUDED__

#include <chrono>
#include <string>
#include <filesystem>
#include <functional>

namespace libwatcher
{
  enum class FileStatus
  {
    initial,
    created,
    modified,
    erased
  };

  class Watcher
  {

  private:
    std::filesystem::path _watched_dir;
    std::chrono::duration<int, std::milli> _delay;
    bool _running = true;
    std::unordered_map<std::string, std::filesystem::file_time_type> _paths;

  public:
    Watcher(std::filesystem::path watched_dir, std::chrono::duration<int, std::milli> delay);
    void start(const std::function<void(std::filesystem::path, FileStatus, std::error_code &)> &action);
  };
}

#endif
