#include <filesystem>
#include <chrono>
#include <functional>
#include <thread>
#include "watcher.hpp"
#include <spdlog/spdlog.h>

libwatcher::Watcher::Watcher(std::filesystem::path watched_dir, std::chrono::duration<int, std::milli> delay) : _watched_dir{watched_dir}, _delay{delay}
{
  if (!std::filesystem::is_directory(_watched_dir))
  {
    throw "Provided path is not a directory";
  }
  for (auto &file : std::filesystem::recursive_directory_iterator(_watched_dir))
  {
    _paths[file.path().string()] = std::filesystem::last_write_time(file);
  }
};

void libwatcher::Watcher::start(const std::function<void(std::filesystem::path, FileStatus, std::error_code &)> &action)
{
  std::error_code err;
  // Loop through initial files
  for (auto it = _paths.begin(); it != _paths.end(); it++)
  {
    action(it->first, FileStatus::initial, err);
  }
  while (_running)
  {
    // Sleep between checks
    std::this_thread::sleep_for(_delay);
    // Detect erased files
    auto it = _paths.begin();
    while (it != _paths.end())
    {
      if (!std::filesystem::exists(it->first))
      {
        action(it->first, FileStatus::erased, err);
        it = _paths.erase(it);
      }
      else
      {
        it++;
      }
    }

    // Detect created and modified files
    for (auto &file : std::filesystem::recursive_directory_iterator(_watched_dir))
    {
      auto current_file_last_write_time = std::filesystem::last_write_time(file);

      // File created
      if (!_paths.contains(file.path().string()))
      {
        _paths[file.path().string()] = current_file_last_write_time;
        action(file.path().string(), FileStatus::created, err);
      }
      // File modified
      else
      {
        if (_paths[file.path().string()] != current_file_last_write_time)
        {
          _paths[file.path().string()] = current_file_last_write_time;
          action(file.path().string(), FileStatus::modified, err);
        }
      }
    }
  }
};
