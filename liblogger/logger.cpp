#include "logger.hpp"
#include <filesystem>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_sinks.h>

std::shared_ptr<spdlog::logger> liblogger::prepare_logger(std::filesystem::path log_file)
{
  std::vector<spdlog::sink_ptr> sinks;
  sinks.push_back(std::make_shared<spdlog::sinks::stdout_sink_st>());
  sinks.push_back(std::make_shared<spdlog::sinks::basic_file_sink_mt>(log_file));
  auto logger = std::make_shared<spdlog::logger>(liblogger::LOGGER_NAME, begin(sinks), end(sinks));
  spdlog::set_level(spdlog::level::info);
  spdlog::register_logger(logger);
  spdlog::flush_every(std::chrono::seconds(2));
  return logger;
}

std::shared_ptr<spdlog::logger> liblogger::get_logger()
{
  return spdlog::get(liblogger::LOGGER_NAME);
};
