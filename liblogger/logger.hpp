#ifndef __LOGGER_H_INCLUDED__
#define __LOGGER_H_INCLUDED__

#include <spdlog/spdlog.h>
#include <filesystem>
#include <string>

namespace liblogger
{
  const std::string LOGGER_NAME = "main";

  std::shared_ptr<spdlog::logger> prepare_logger(std::filesystem::path log_file);

  std::shared_ptr<spdlog::logger> get_logger();
}

#endif
